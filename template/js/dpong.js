// dpong.js

$(document).ready(function() {
	dpong.init();
});

//форма PHP
dpong = {


//Сохранение при нажатие на Enter
enterSave: function(focusId,func){
    $(focusId).focus(function(){
        $(focusId).keypress(function(e){
            if(e.keyCode==13){
                var val = $(focusId).val();        
        		func(val);
            }
        });
    });

    $(focusId).blur(function(){
        var val = $(focusId).val();
        func(val);
    });
},

//Сохранение при изменении
changeSave: function(focusId,func){
    $(focusId).change(function(){
        var val = $(focusId).val();
        func(val);
    });
},


server:{

    router:"",

	post:function (action,post,func) {

		if (!post){
			post={action:action};
		} else {
			post.action = action;
		}

		$.post(dpong.server.router, post)
		.done(function(data) {

  		var parseData=data.split(':::');
	  		if(parseData[1]){
	  			var nameFunc = parseData[0].trim();
	  			processResult = dpong[nameFunc](parseData[1]);
	  			if (func) func(processResult);
	  		} else {
	  			if (func) func(parseData[0]);
	  		}
		})
		.fail(function() { alert("Ошибка асинронного запроса"); });
	}
},


init:function (){
	$(".dp-submit").click(function () {
		var value = $(this).data("value");
		var group = $(this).data("group");
		var result = $(this).data("result");
		var processor = $(this).data("processor");
		var preprocessor = $(this).data("preprocessor");
		var action = $(this).data("action")||null;

		if (preprocessor){
		dpong[preprocessor]("#"+result);
		}

		//post = dpong.server.post(action,value,group);
		//$("#"+result).html(post);

		var post={action:action};
		$("."+group).each(function () {
			post[$(this).attr('name')] = $(this).val();
		}); 

		if (value) {
			post["value"]=value;
		}
        
		$.post(dpong.server.router, post)
		.done(function(data) {
  		var parseData=data.split(':::');
	  		if(parseData[1]){
	  			var nameFunc = parseData[0].trim();
	  			processResult = dpong[nameFunc]("#"+result,parseData[1]);
	  			$("#"+result).html(processResult);
	  		} else {
	  			$("#"+result).html(parseData[0]);
	  		}
		})
		.fail(function() { alert("Ошибка асинронного запроса"); });

		if (processor){
		dpong[processor]("#"+result);
		}
	}); 

//Форма JS
	$(".dp-button").click(function () {
		var group = $(this).data("group");
		var value = $(this).data("value");
		var result = $(this).data("result");
		var processor = $(this).data("processor");


if (group) {
	value = {};
	value['value']=$(this).data("value");
		$("."+group).each(function () {
			value[$(this).attr('name')] = $(this).val();
		}); 
}
		dpong[processor]("#"+result,value);
	}); 



//Форма Перетаскивания
  $('.dp-drug').draggable({
  		opacity: 0.7,
  		distance: 50,
  		zIndex: 9999,
        helper : 'clone',
        start: function () {
        	if ($(this).hasClass('dp-drug--zoom-out')) {
        	$(".ui-draggable-dragging").addClass('zoom-out');
        	}
        },
                

        });

    $('.dp-drop').droppable({        
                drop : function(event, ui) {
                  var data={};
                  var drug = ui.draggable.data("drug").split('::'); data.drug = drug;
                   data.drug.this = ui.draggable;
                  var drop = $(this).data("drop").split('::'); data.drop = drop;
                   data.drop.this = $(this);
                  var processor = $(this).data("ondrop");
                  	if(processor) {
                        dpong[processor](data);
                  	} else {
                  		alert('Ошибка, обработчик события ondrop() не определен');
                  	}
                }
        });

}


}
