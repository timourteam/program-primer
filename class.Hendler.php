<?
class Hendler {

	 //тут лежит наша основная программа, в нее передается пример, который надо решить
	 public function clcMath($primer) {

	 //чтобы не тратить оперативную память под экземпляр класса Math, содержащий наши функции(потому что помимо функций в нем ничего не будет), мы будем вызвать из класса его методы через ::

	 	define("OPERATIONS",["+","-","*","/"]);

	 	 $prim = $primer;

		//нашли позиции всех умножений и делений
		$umn = Math::searchAction($prim , "*");
		$min = Math::searchAction($prim , "/");
		$znpozs = Math::solution($umn , $min);

		//выполняем их последовательно
		$offset = 0;
		foreach ($znpozs as $znpoz) {
			$tmp_res = Math::play($prim , $znpoz-$offset);
			$prim = $tmp_res[0];
			$offset += strlen($tmp_res[1]) - strlen($tmp_res[2]);
		}


		//аналогично для сложения и вычитания
		$plus = Math::searchAction($prim, "+");
		$mins = Math::searchAction($prim, "-");
		$znpozs1 = Math::solution($plus , $mins);

		$offset = 0;
		foreach ($znpozs1 as $znpoz1) {
			$tmp_res = Math::play($prim , $znpoz1-$offset);
			$prim = $tmp_res[0];
			$offset += strlen($tmp_res[1]) - strlen($tmp_res[2]);
		}

		//возвращаем результат
		return $prim ;
	}

}

?>